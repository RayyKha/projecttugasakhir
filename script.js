const header = document.querySelector("header");
window.addEventListener ("scroll", function(){
    header.classList.toggle ("sticky", window.scrollY > 100);
});

let menu = document.querySelector('#menu-icon');
let navlist = document.querySelector('.navlist');

menu.onclick = () => {
    menu.classList.toggle('bx-x');
    navlist.classList.toggle('open');
};

window.onscroll = () => {
    menu.classList.remove('bx-x');
    navlist.classList.remove('open');
};

// Mendapatkan referensi elemen-elemen pada formulir
const form = document.querySelector('form');
const fullnameInput = document.getElementById('fullname');
const emailInput = document.getElementById('email');
const passwordInput = document.getElementById('password');
const dateInput = document.getElementById('date');
const genderInput = document.getElementById('gender');

// Array untuk menyimpan entri formulir
const entries = [];

// Menambahkan event listener untuk menangani pengiriman formulir
form.addEventListener('submit2', function(event) {
  // event.preventDefault(); // Menghentikan pengiriman formulir

  // Mendapatkan nilai dari input
  const fullnameValue = fullnameInput.value;
  const emailValue = emailInput.value;
  const passwordValue = passwordInput.value;
  const dateValue = dateInput.value;
  const genderValue = genderInput.value;

  // Validasi data
  if (fullnameValue === '') {
    alert('Silakan masukkan nama lengkap Anda.');
    return;
  }

  if (emailValue === '') {
    alert('Silakan masukkan alamat email Anda.');
    return;
  }

  if (passwordValue === '') {
    alert('Silakan masukkan password Anda.');
    return;
  }

  if (dateValue === '') {
    alert('Silakan pilih tanggal lahir Anda.');
    return;
  }

  if (genderValue === '') {
    alert('Silakan pilih jenis kelamin Anda.');
    return;
  }

  // Menambahkan entri formulir ke dalam array
  const entry = {
    fullname: fullnameValue,
    email: emailValue,
    password: passwordValue,
    date: dateValue,
    gender: genderValue
  };
  entries.push(entry);

  // Mengosongkan input setelah mengirim formulir
  fullnameInput.value = '';
  emailInput.value = '';
  passwordInput.value = '';
  dateInput.value = '';
  genderInput.value = '';

  // Menampilkan entri formulir yang telah dikirim
  console.log(entries);
});


