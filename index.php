<?php
session_start();

function connectToDatabase() {
    $koneksi = mysqli_connect("localhost", "root", "", "form");
    if (!$koneksi) {
        die("Connection failed: " . mysqli_connect_error());
    }
    return $koneksi;
}

function insertData($data) {
    $koneksi = connectToDatabase();
    
    $nama = $data['nama'];
    $gender = $data['gender'];
    $email = $data['email'];
    $subject = $data['subject'];
    $pesan = $data['pesan'];
    
    $query = "INSERT INTO form (nama, gender, email, subject, pesan) VALUES ('$nama', '$gender', '$email', '$subject', '$pesan')";
    if (mysqli_query($koneksi, $query)) {
        echo "
        <script>
        alert('Berhasil Dikirim');
        document.location.href = 'index.php';
        </script>
        ";
    } else {
        echo "Error: " . $query . "<br>" . mysqli_error($koneksi);
    }
    
    mysqli_close($koneksi);
}

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    // Memeriksa apakah variabel session 'form_entries' sudah ada
    if (!isset($_SESSION['form_entries'])) {
        $_SESSION['form_entries'] = [];
    }

    // Proses pengiriman formulir dan penyimpanan data ke database
    $nama = $_POST['nama'];
    $gender = $_POST['gender'];
    $email = $_POST['email'];
    $subject = $_POST['subject'];
    $pesan = $_POST['pesan'];

    insertData($nama, $gender, $email, $subject, $pesan);

    // Menambahkan entri formulir ke dalam variabel session 'form_entries'
    $entry = [
        'nama' => $nama,
        'gender' => $gender,
        'email' => $email,
        'subject' => $subject,
        'pesan' => $pesan
    ];
    $_SESSION['form_entries'][] = $entry;
}
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Web Personal</title>
    <!-- custom css link -->
    <link rel="stylesheet" type="text/css" href="style.css" />

    <link
      rel="stylesheet"
      href="https://unpkg.com/boxicons@latest/css/boxicons.min.css"
    />

    <link rel="preconnect" href="https://fonts.googleapis.com" />
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
    <link
      href="https://fonts.googleapis.com/css2?family=Poppins:wght@300;400;500;600;700;800;900&display=swap"
      rel="stylesheet"
    />
  </head>
  <body>
    <!--header design  -->
    <header>
      <a href="#" class="logo">Cod<span>e</span>r.</a>

      <ul class="navlist">
        <li><a href="#home" class="active">Home</a></li>
        <li><a href="#about">About me</a></li>
        <li><a href="#services">Services</a></li>
        <li><a href="#portfolio">Portfolio</a></li>
        <li><a href="#contact">Contact</a></li>
        <li><a href="#Community">Community</a></li>
      </ul>

      <div class="bx bx-menu" id="menu-icon"></div>
    </header>

    <!--home section design  -->
    <section class="home" id="home">
      <div class="home-text">
        <div class="slide">
          <span class="one">Hello</span>
          <span class="two">I'm</span>
        </div>
        <h1>Rayyan</h1>
        <h3>Front-End <span>Developer.</span></h3>
        <p>
          Transformasikan ide-ide Anda menjadi <br />tampilan web yang memikat
          dengan desain yang <span>kreative</span>.
        </p>
        <div class="button">
          <a href="#" class="btn">Say Hello</a>
          <!-- <a href="#" class="btn2"><span><i class='bx bx-play'></i></span>Wacth
                    My Work</a> -->
        </div>
      </div>
    </section>

    <!--about section design  -->
    <section class="about" id="about">
      <div class="about-img">
        <img src="about.jpg" />
      </div>

      <div class="about-text">
        <h2>About <span>Me</span></h2>
        <h4>Web Developer</h4>
        <p>
          Saya adalah seorang mahasiswa semester dua diprogram studi
          Informatika, dengan keahlian utama dibidang Web Developer. selain itu,
          saya juga memiliki keahlian dibidang visual design dan Web Design.
        </p>
        <!-- <a href="#" class="btn">More About</a> -->
      </div>
    </section>

    <!--services section design  -->
    <section class="services" id="services">
      <div class="main-text">
        <h2><span>My</span> Skills</h2>
      </div>

      <div class="services-content">
        <div class="box">
          <!-- <div class="s-icons">
                    <i class='bx bx-mobile-alt'></i>
                </div> -->
          <img src="c++.png" />
          <!-- <a href="#" class="read">Read More</a> -->
        </div>

        <div class="box">
          <!-- <div class="s-icons">
                    <i class='bx bx-code-alt'></i>
                </div> -->
          <img src="php.png" />
          <!-- <a href="#" class="read">Read More</a> -->
        </div>

        <div class="box">
          <!-- <div class="s-icons">
                    <i class='bx bx-edit-alt'></i>
                </div> -->
          <img src="js.png" />
          <!-- <a href="#" class="read">Read More</a> -->
        </div>
      </div>
    </section>

    <!--portfolio section design  -->
    <section class="portfolio" id="portfolio">
      <div class="main-text">
        <p>Portfolio</p>
        <h2><span>Latest</span> Project</h2>
      </div>

      <div class="portfolio-content">
        <div class="row">
          <img src="portfolio1.jpg" />
          <div class="layer">
            <h5>Visual Design</h5>
            <p>
              Membuat Art dengan ide yang kreative, sehingga menjadi visual
              design yang menarik.
            </p>
            <!-- <a href="#"><i class='bx bx-link-external'></i></a> -->
          </div>
        </div>

        <div class="row">
          <img src="portfolio2.jpg" />
          <div class="layer">
            <h5>Visual Design</h5>
            <p>
              Membuat Art dengan ide yang kreative, sehingga menjadi visual
              design yang menarik.
            </p>
            <!-- <a href="#"><i class='bx bx-link-external'></i></a> -->
          </div>
        </div>

        <div class="row">
          <img src="portfolio3.jpg" />
          <div class="layer">
            <h5>Visual Design</h5>
            <p>
              Membuat Art dengan ide yang kreative, sehingga menjadi visual
              design yang menarik.
            </p>
            <!-- <a href="#"><i class='bx bx-link-external'></i></a> -->
          </div>
        </div>

        <div class="row">
          <img src="portfolio4.png" />
          <div class="layer">
            <h5>Web Design</h5>
            <p>Membuat web responsive menggunakan html, css, js, php.</p>
            <!-- <a href="#"><i class='bx bx-link-external'></i></a> -->
          </div>
        </div>

        <div class="row">
          <img src="portfolio5.png" />
          <div class="layer">
            <h5>Web Design</h5>
            <p>Membuat web responsive menggunakan html, css, js, php.</p>
            <!-- <a href="#"><i class='bx bx-link-external'></i></a> -->
          </div>
        </div>

        <div class="row">
          <img src="portfolio6.png" />
          <div class="layer">
            <h5>Web Design</h5>
            <p>Membuat web responsive menggunakan html, css, js, php.</p>
            <!-- <a href="#"><i class='bx bx-link-external'></i></a> -->
          </div>
        </div>
      </div>
    </section>

    <!-- contact section design -->
    <section class="contact" id="contact">
      <div class="contact-text">
        <h2>Contact <span>Me!</span></h2>
        <h4>Jika Anda Berfikir Ingin Memiliki Project.</h4>
        <p>
          Saya sebagai Web Development - Membuat desain web & berani untuk
          perusahaan di seluruh dunia.</p>
        <div class="list">
          <li>
            <a href="#"><i class="bx bx-phone"></i> 081275743225</a>
          </li>
          <li>
            <a href="#"
              ><i class="bx bx-envelope"></i> riankhadafi21@gmail.com</a>
          </li>
        </div>

        <div class="contact-icons">
          <a href="#"><i class="bx bxl-instagram-alt"></i></a>
          <a href="#"><i class="bx bxl-tiktok"></i></a>
          <a href="#"><i class="bx bxl-youtube"></i></a>
        </div>
      </div>
      
      <div class="contact-form">
        <form action="index.php" method="POST">
          <input type="text" name="nama" placeholder="Nama" required />
          <input
            type="text" name="gender" placeholder="Jenis Kelamin" required/>
          <input type="email" name="email" placeholder="Alamat Email" required/>
          <input type="text" name="subject" placeholder="Subject" required />
          <textarea name="pesan" id="phone" cols="35" rows="10" placeholder="Pesan" required ></textarea>
          <input type="submit" name="submit" value="send" class="submit" required/>
        </form>
        <div class="perulangan">
       <?php
       // Menampilkan informasi pengisian formulir
       if (isset($_SESSION['form_entries']) && count($_SESSION['form_entries']) > 0) {
           echo "<h5>Anda telah mengisi formulir sebanyak <span> " . count($_SESSION['form_entries']) . "</span> kali.</h5>";
       }
       ?>
        </div>
      </div>
       
      </div>
    </section>
    
    <!-- form validation -->
    <section class="validation">
      <div class="validation-join">
        <form action="" id="Community">
          <h2>Join Community</h2>
            <label for="fullname">Nama Lengkap</label>
            <input type="text" id="fullname" placeholder="Masukan Nama Lengkap Anda" required>
            <label for="email">Alamat Email</label>
            <input type="text" id="email" placeholder="Masukan ALamat Email Anda" required>
            <label for="password">Password</label>
            <input type="password" id="password" placeholder="Masukan password Anda" required>
            <i id="pass-toggle-btn" class="fa-solid fa-eye"></i>
            <label for="date">Tanggal Lahir</label>
            <input type="date" id="date" placeholder="Pilih" required min="1900-01-01" max="<?php echo date('Y-m-d'); ?>">
            <label for="gender">Jenis Kelamin</label>            
            <select id="gender" required>
              <option value="" selected disabled>Pilih</option>
              <option value="Male">Laki-Laki</option>
              <option value="Female">Perempuan</option>
              <option value="Other">Lainnya</option>
            </select>
            <input type="submit" id="submitt" value="Submit" class="submit" required>
        </form>
      </div>
    </section>

    <!-- end section design -->
    <section class="end">
      <div class="last-text">
        <p>Copyright © 2023 by M Rayyan Khadafi All right Reserved.</p>
      </div>

      <div class="top">
        <a href="#home"><i class="bx bx-up-arrow-alt"></i></a>
      </div>
    </section>

    <!-- custom js link -->
    <script type="text/javascript" src="script.js"></script>
  </body>
</html>
